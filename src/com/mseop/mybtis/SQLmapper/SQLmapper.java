package com.mseop.mybtis.SQLmapper;

public class SQLmapper {

	public static void main(String[] args) {
		
		
		
		/* myBatis 설정파일
			Mybatis의 사용 목적중 하나 [ DAO로 부터 SQL문을 분리하는것,]
			분리된 SQL문은 SQLMapper 파일에 작성하고, DAO에서는 SqlSession 객체가 SQLmapper파일을 참조한다.
			
			
			[ 속성 ]
			
			id : 각 SQL문을 구분
			resultType : SELECT문 실행 결과를 담을 객체, 패키지 이름을 포함한 클래스 이름 또는 객체 alias 지정
			resultMap : SELECT문 실행 결과를 담을 객체를 resultMap으로 지정, Type & Map 중 하나를 택해서 설장한다.
			parameterType : 이 속성에 지정한 객체의 프로퍼티값이 SQL문의 입력 파라미터에 지정 됨. 
			
			
			------------------------------------resultType 속성------------------------------------
			
			
			+ SELECT문을 실행하면 결과가 생성되는데, 이 결과를 담을 객체를 resultType 속성에 지정,
			resultType에는 ( 패키지 이름 포함 전체 클래스명 지정 & 객체의 alias )를 지정 가능
			alias는 MyBatis 설정파일에 설정!
			
			패키지 이름을 포함한 전체 클래스명 지정
			<select id="selectList" resultMap="com.atoz_develop.spms.vo.Project">
			</select>
			
			<!-- MyBatis 설정파일 -->
			<typeAliases>
		    	<typeAliase type="com.atoz_develop.spms.vo.Project" alias="project"/>
			</typeAliases>
			 
			<!-- SQL Mapper 파일 -->
			<select id="selectList" resultMap="project">
			</select>

			
			------------------------------------resultMap 속성------------------------------------
			
			resultType 속성을 사용하면 setter와 매칭되지 않는 경우 각 컬럼마다 alias를 붙여야하는 번거로움 발생,
			이를해결 하기 위해 resultMap 속성을 이용하면 문제 해결이 된다.
			
			<resultMap type="project" id="projectResultMap">
				<id column="PNO" property="no"/>
				<result column="PNAME" property="title" />
			</resultMap>	
			
			-------풀이-------
			<resultMap>.type: SELECT 결과를 저장할 클래스 이름 또는 MyBatis 설정파일에 설정된 alias
			<resultMap>.id: resultMap의 id
			<id>: 객체 식별자로 사용되는 프로퍼티
			<id>.column: 컬럼명
			<id>.property: 객체 프로퍼티명(setter 메소드 이름에서 set을 빼고 첫 알파엣을 소문자로 만든 이름)
			<result>: 컬럼-setter 연결 정의
			<result>.column: 컬럼명
			<result>.property: 객체 프로퍼티명(setter 메소드 이름에서 set을 빼고 첫 알파엣을 소문자로 만든 이름)
			<result>.javaType: 컬럼 값을 특정 자바 객체로 변환할때 사용
			
			
			
			
			
			
			
			
			
			
			
		 */
		
		
		
	}
	
	
}






































